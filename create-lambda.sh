#!/bin/sh

profile=${profile:-"default"}

while [ $# -gt 0 ]; do
	if [[ $1 == *"--"* ]]; then
		param="${1/--/}"
		declare $param="$2"
	fi
	shift
done

echo "What is the name of the function you'd like to create (Note: it must start with \"dev\")?"
read FUNCTION_NAME

echo "Building the binary..."
. ./build.sh

echo "Creating function '$FUNCTION_NAME' using '$profile' profile..."
aws --profile $profile lambda create-function \
	--function-name $FUNCTION_NAME \
	--runtime go1.x \
	--role arn:aws:iam::883030985124:role/devLambdaExecRole-MesabiDefault \
	--handler main \
	--zip-file fileb://main.zip

echo "All done!"