package main

import (
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Book struct
type Book struct {
	Name          string `json:"name,omitempty"`
	Isbn          int    `json:"isbn,omitempty"`
	BookEdition   string `json:"bookEdition,omitempty"`
	Price         int    `json:"price,omitempty"`
	PriceCurrency string `json:"priceCurrency,omitempty"`
	Availability  string `json:"availability,omitempty"`
	Units         int    `json:"units,omitempty"`
	URL           string `json:"url,omitempty"`

	EligibleRegion []EligibleRegion `json:"eligibleRegion,omitempty"`
	Author         Author           `json:"author,omitempty"`
}

// EligibleRegion struct
type EligibleRegion struct {
	Country string `json:"country,omitempty"`
}

// Author struct
type Author struct {
	FirstName  string `json:"firstName,omitempty"`
	MiddleName string `json:"middleName,omitempty"`
	LastName   string `json:"lastName,omitempty"`
	URL        string `json:"url,omitempty"`
	About      string `json:"about,omitempty" validate:"nonzero"`
}

// Handler handles requests
func Handler(request events.APIGatewayProxyRequest) ([]Book, error) {
	// Log request as it came in
	log.Printf("REQUEST: %v", request)

	// Get books from database
	book := []Book{
		{
			Name:        "Go Programming Language",
			BookEdition: "1st Edition",
			EligibleRegion: []EligibleRegion{
				{
					Country: "USA",
				},
				{
					Country: "France",
				},
				{
					Country: "England",
				},
			},
			Author: Author{
				FirstName:  "Alan",
				MiddleName: "A.",
				LastName:   "Donovan",
			},
		},
		{
			Name:        "Mastering Go",
			BookEdition: "Second Edition",
			EligibleRegion: []EligibleRegion{
				{
					Country: "USA",
				},
				{
					Country: "France",
				},
				{
					Country: "Brazil",
				},
			},
			Author: Author{
				FirstName: "Mihalis",
				LastName:  "Tsoukalos",
				// About:     "is an accomplished author. His previous books, Go Systems Programming and Mastering Go, have become a must-read for the Unix and Linux systems professionals. When not writing books, he spends his working life as a Unix administrator, programmer, DBA, and mathematician who enjoys writing technical articles and learning new technologies. His research interests include programming languages, visualization, and databases. He holds a BSc in Mathematics from the University of Patras and an MSc in IT from University College London, UK. He has written various technical articles for Sys Admin, MacTech, C/C++ Users Journal, Linux Journal, Linux User and Developer, Linux Format, and Linux Voice.",
			},
		},
	}
	return book, nil
}

func main() {
	lambda.Start(Handler)
}
