module bitbucket.org/ecwdevs/mesabi-lambda-devgetbook

go 1.14

require (
	github.com/aws/aws-lambda-go v1.16.0
	github.com/aws/aws-sdk-go v1.31.7
	github.com/go-playground/validator/v10 v10.3.0
	gopkg.in/validator.v2 v2.0.0-20200605151824-2b28d334fa05
)
