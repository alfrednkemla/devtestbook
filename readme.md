# Mesabi Lambda Microservice Template

This is a starter template for creating new Lambda functions. It is set up by default to receive an `APIGatewayProxyRequest` and return an `APIGatewayProxyResponse`, which means that the API Gateway method wired to this Lambda is meant to have an Integration Request configured as such:

* Integration type: Lambda Function
* Use Lambda Proxy integration: checked

In other word, in the API Gateway service of the AWS Console, this is what you should see:

![API Gateway](images/api-gateway-integration-request.png)

## How to Use this Template Repo

### Two important notes before beginning

1. Both `create-lambda.sh` and `update-lambda.sh`, mentioned below, assume you have the AWS CLI installed and configured in your dev environment. If you do not, you must do that before you run either script. See [this page](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) for instructions on how to install and configure it on your system.
1. If your Ecreative AWS account is not your `default` AWS profile as defined in `~/.aws/credentials`, when you run either `create-lambda.sh` or `update-lambda.sh` you will need to pass a named option (`--profile`) specifying which named profile the command should use. E.g. `./create-lambda.sh --profile ecw`

### Getting Started

1. Clone this repository to your development environment (make sure tp replace `name-of-repo` with the name of the repo you are creating): `git clone git@bitbucket.org:ecwdevs/mesabi-template.git name-of-repo`
1. Update the `main.go` file until it satisfies your requirements.
1. Run `./build.sh` to ensure there are no compilation errors.
1. When you are ready to push your code to AWS, run `./create-lambda.sh`. This should only need to be run once. Note that you will be prompted for a name for your Lambda. **_Important: This name must start with the string 'dev' and should be camelcased (e.g. devUserGet)._**
1. Having run `./create-lambda.sh`, open `update-lambda.sh` and update the line `functionname=""` so that the name of the function you created in step 4 is assigned to the vaiable `functionname`.
1. Push changes to AWS by running `./update-lambda.sh`.
1. Commit and push to Bitbucket as you normally would.

### Testing

1. In the `main_test.go`, there is a `TestLambda` function. Change the `functionName` to the lambda function that needs to be tested.
2. Run `go test` to test the lambda function. You should see a `PASS` in your console if everything went well.
