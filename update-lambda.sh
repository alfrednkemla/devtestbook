#!/bin/sh

profile=${profile:-"default"}
functionname="devTestBook"

while [ $# -gt 0 ]; do
	if [[ $1 == *"--"* ]]; then
		param="${1/--/}"
		declare $param="$2"
	fi
	shift
done

if [[ $functionname == "" ]]; then
	echo "You must set 'functionname' in $0."
	exit
fi

echo "Using profile $profile to update function $functionname..."

echo "Building the binary..."
. ./build.sh

echo "Updating function on AWS..."
aws --profile $profile lambda update-function-code \
	--function-name $functionname \
	--zip-file fileb://main.zip

echo "All done!"