package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"gopkg.in/validator.v2"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
)

func TestLambda(t *testing.T) {
	// Declare the lambda function's name.
	functionName := aws.String("devTestBook")
	// Declare the lambda function's request properties.
	input := events.APIGatewayProxyRequest{HTTPMethod: http.MethodGet}
	// Declare the lambda function's returning struct.
	var book []Book
	// Marshal the request or fail otherwise.
	payload, err := json.Marshal(input)
	if err != nil {
		t.Errorf("Error: Marshalling the request failed.")
	}
	// Create an AWS SDK session with the shared profile.
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	// Use the AWS SDK session to create a lambda service.
	svc := lambda.New(sess)
	// Use the lambda service to invoke the lambda function with the lambda function's name and the payload.
	result, err := svc.Invoke(&lambda.InvokeInput{FunctionName: functionName, Payload: payload})
	if err != nil {
		t.Errorf("Error: The %q invocation failed.", *functionName)
	}
	// Unmarshal the result payload into the book struct.
	err = json.Unmarshal(result.Payload, &book)
	if err != nil {
		t.Errorf("Error: Unmarshalling the result payload failed.")
	}
	// Validate the returned book.
	validateErrors := validator.Validate(book)
	if validateErrors == nil {
		fmt.Println("Values are valid")
	} else {
		// Loop trough each book and check if the struct annotations are valid.
		errs := validateErrors.(validator.ErrorMap)
		for f, e := range errs {
			t.Errorf("\t - %s (%v)\n", f, e)
		}
	}

}
